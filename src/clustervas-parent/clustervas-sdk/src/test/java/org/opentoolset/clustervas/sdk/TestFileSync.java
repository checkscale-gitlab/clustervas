package org.opentoolset.clustervas.sdk;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.Arrays;
import java.util.List;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.client.session.forward.PortForwardingTracker;
import org.apache.sshd.common.config.keys.KeyUtils;
import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.apache.sshd.common.util.security.SecurityUtils;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.forward.ForwardingFilter;
import org.junit.Test;

import com.github.fracpete.processoutput4j.output.CollectingProcessOutput;
import com.github.fracpete.rsync4j.RSync;

public class TestFileSync {

	@Test
	public void test() throws Exception {
		KeyPairGenerator keyPairGenerator = SecurityUtils.getKeyPairGenerator(KeyUtils.RSA_ALGORITHM);
		List<KeyPair> keyPairs = Arrays.asList(keyPairGenerator.generateKeyPair());
		SshServer sshd = SshServer.setUpDefaultServer();
		sshd.setHost("127.0.0.1");
		sshd.setPort(8022);
		sshd.setKeyPairProvider(session -> keyPairs);
		sshd.setPasswordAuthenticator((username, password, session) -> true);
		sshd.setForwardingFilter(new ForwardingFilter() {

			@Override
			public boolean canListen(SshdSocketAddress address, Session session) {
				return false;
			}

			@Override
			public boolean canConnect(Type type, SshdSocketAddress address, Session session) {
				if (!Type.Direct.equals(type)) {
					return false;
				}

				try {
					InetSocketAddress inetSocketAddress = address.toInetSocketAddress();
					InetAddress inetAddress = inetSocketAddress.getAddress();
					int port = inetSocketAddress.getPort();
					if (inetAddress.isLoopbackAddress() && port == 873) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				return false;
			}

			@Override
			public boolean canForwardX11(Session session, String requestType) {
				return false;
			}

			@Override
			public boolean canForwardAgent(Session session, String requestType) {
				return false;
			}
		});

		sshd.start();

		// ---

		SshClient client = SshClient.setUpDefaultClient();
		client.start();
		try (ClientSession session = client.connect("hadi", "localhost", 8022).verify(10000).getSession()) {
			session.auth().verify(10000);
			SshdSocketAddress local = new SshdSocketAddress("localhost", 1873);
			SshdSocketAddress remote = new SshdSocketAddress("localhost", 873);
			try (PortForwardingTracker tracker = session.createLocalPortForwardingTracker(local, remote)) {
				RSync rsync = new RSync().source("rsync://localhost:1873/clustervas").destination("/home/hadi/clustervas/sync/dst").recursive(true);
				CollectingProcessOutput output2 = rsync.execute();
				System.out.println(output2.getStdOut());
				System.out.println("Exit code: " + output2.getExitCode());
				if (output2.getExitCode() > 0) {
					System.err.println(output2.getStdErr());
				}
			}
		}
	}

	// ---
}
